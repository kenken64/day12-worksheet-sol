require('dotenv').config();
const express = require('express'),
      bodyParser = require('body-parser'),
      path = require('path');

const app = express();
const APP_PORT = process.env.PORT;
const MYSQL_USER = process.env.MYSQL_USER;
console.log("APP_PORT : " + APP_PORT);
console.log("MYSQL_USER : " + MYSQL_USER);

//app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
// create application/json parser
//const jsonParser = bodyParser.json()
// create application/x-www-form-urlencoded parser
//const urlencodedParser = bodyParser.urlencoded({ extended: false })
console.log(__dirname);
const angularDistPath = path.join(__dirname, '../', 'dist', 'day12-worksheet');
console.log(angularDistPath);
app.use(express.static(angularDistPath));

app.get('/api/customer', (req,res)=>{
    console.log("Fetch customer ...");
    res.status(200).json({name: 'Kenneth Phang3'});
});

app.post('/api/customer', (req, res)=>{
    console.log('Save customer ...')
    console.log(req.body);
    res.status(200).json({});
})

app.listen(APP_PORT, ()=>{
    console.log("App server started at " + APP_PORT);
})