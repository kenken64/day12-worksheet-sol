import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  API_TEST: string = "/api/customer";

  constructor(private httpSvc: HttpClient) { }

  getCustomer(): Promise<any>{
    return this.httpSvc.get(this.API_TEST).toPromise();
  }

  getCustomerObservable(): Observable<any>{
    return this.httpSvc.get(this.API_TEST);
  }

  saveCustomer(): Observable<any>{
    let customer = {
      name: 'Kenneth',
      age: 20
    }
    return this.httpSvc.post(this.API_TEST, customer);
  }
}
