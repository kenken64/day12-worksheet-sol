import { Component, OnInit } from '@angular/core';
import { CustomerService } from './services/customer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'day12-worksheet';
  resultInStr: string;
  resultInStrOb: string;
  
  constructor(private custSvc: CustomerService){}

  ngOnInit(){
    this.custSvc.getCustomer().then(result=>{
      console.log(result);
      this.resultInStr = result.name;
    })

    this.custSvc.getCustomerObservable().subscribe(result=>{
      console.log(result);
      this.resultInStrOb = result.name;
    })

    this.custSvc.saveCustomer().subscribe((result)=>{
      console.log(result);
    });
  }
}
